.. _contributing:

============
Contributing
============

If you would like to contribute to the development of the networking-dpm
project, you must follow the rules for OpenStack contributions described in
the "If you're a developer, start here" section of this page:

   https://wiki.openstack.org/wiki/How_To_Contribute

Once those steps have been completed, changes to the networking-dpm project
should be submitted for review via the Gerrit tool, following the workflow
documented at:

   https://docs.openstack.org/infra/manual/developers.html

Pull requests submitted through GitHub will be ignored.

The Git repository for the networking-dpm project is here:

    https://git.openstack.org/cgit/openstack/networking-dpm

Bugs against the networking-dpm project should be filed on Launchpad (not on
GitHub):

    https://bugs.launchpad.net/networking-dpm

Pending changes for the networking-dpm project can be seen on its Gerrit page:

    https://review.openstack.org/#/q/project:openstack/networking-dpm
