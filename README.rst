================================
openstack/networking-dpm Project
================================

About this project
------------------

This project provides the OpenStack Neutron mechanism driver and L2 agent for
the PR/SM hypervisor of IBM z Systems and IBM LinuxOne machines that are in the
DPM (Dynamic Partition Manager) administrative mode.

The DPM mode enables dynamic capabilities of the firmware-based PR/SM
hypervisor that are usually known from software-based hypervisors, such as
creation, deletion and modification of partitions (i.e. virtual machines) and
virtual devices within these partitions, and dynamic assignment of these
virtual devices to physical I/O adapters.

Links
-----

* Documentation: `<http://networking-dpm.readthedocs.io/en/latest/>`_
* Source: `<https://git.openstack.org/cgit/openstack/networking-dpm>`_
* Github shadow: `<https://github.com/openstack/networking-dpm>`_
* Bugs: `<http://bugs.launchpad.net/networking-dpm>`_
* Gerrit: `<https://review.openstack.org/#/q/project:openstack/networking-dpm>`_
* License: Apache 2.0 license
