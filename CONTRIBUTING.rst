If you would like to contribute to the development of the OpenStack project,
please read the `contributing`_ section of its documentation.

.. _contributing: http://networking-dpm.readthedocs.io/en/latest/contributing.html#contributing
